#!/usr/bin/python3

import sys
import os
__author__ = 'robert'


def gbk2utf8(input_file, output_file):
    if not os.path.isfile(input_file):
        print("[Error]\t" + input_file + " is not a valid file.")
        usage()
        exit(-1)
    file_obj = open(input_file, encoding="gbk")

    data = file_obj.read()

    write_obj = open(output_file, "w")
    write_obj.write(str(data))

    file_obj.close()
    write_obj.close()

    print("[Info]\tConvert Success!!!")


def usage():
    print("\nUsage:\npython3 " + sys.argv[0] + " input_file output_file\n")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        print("[Info]\tInput file: " + sys.argv[1])
        if len(sys.argv) > 2:
            print("[Info]\tOutput file: " + sys.argv[2])
        else:
            print("[Error]\tOutput file name is not assigned.\n")
            usage()
            exit(-1)
    else:
        print("[Error]\tNo Enough Parameters\n")
        usage()
        exit(-1)

    gbk2utf8(sys.argv[1], sys.argv[2])


